# NlRubocopRules

Add to project `.rubocop.yml`:

```
inherit_gem:
  nl_rubocop_rules: .rubocop.yml

inherit_mode:
  merge:
    - Include
    - Exclude
```
