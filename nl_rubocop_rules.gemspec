# frozen_string_literal: true

lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "nl_rubocop_rules/version"

Gem::Specification.new do |spec|
  spec.name          = "nl_rubocop_rules"
  spec.version       = NlRubocopRules::VERSION
  spec.authors       = ["Nils Landt"]
  spec.email         = ["nils.landt@nlsoft.de"]

  spec.summary       = "collection of rubocop rules"
  spec.description   = "collection of rubocop rules"
  spec.homepage      = "https://gitlab.com/promisedlandt/nl_rubocop_rules"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    fail "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
  spec.required_ruby_version = ">= 3.0.0"

  spec.add_dependency "rubocop", "1.33.0"
  spec.add_dependency "rubocop-performance", "1.9.2"

  spec.add_development_dependency "bundler"
end
